# -*- coding: utf-8 -*-

import pandas as pd
import sys
sys.path
import pyarrow.feather as pf
from src.util.results2folder import attach_results,print2folder

from _classes import SSDC


#### Data load ####
## Constants
# Declaration of all categorical and numeric columns (not boolean)
not_boolean_columns = ['finale_covid_status', 'nice_age', 'gender', 'verschillende_ics', 'ref_spec', 'adm_source',
                       'adm_type', 'heartrate_min', 'heartrate_max', 'resprate_min', 'resprate_max', 'syst_min',
                       'syst_max', 'meanbl_min', 'meanbl_max', 'temp_min', 'temp_max', 'urine_24', 'paco2', 'fio2',
                       'nice_pao2_fio2', 'pao2', 'ph_min', 'wbc_min', 'wbc_max', 'creat_min', 'creat_max',
                       'potas_min', 'potas_max', 'sodium_min', 'sodium_max', 'bicarb_min', 'bicarb_max', 'urea',
                       'ht_min', 'ht_max', 'throm_min', 'gluc_min', 'gluc_max', 'nice_a_ado2', 'nice_gcs_low',
                       'los_pre', 'bmi', 'aantalchronisch', 'golf', 'dood']
# load data
datasetname= "220217-134103-results"
delim = ','
filepath =  "./data/" + datasetname + ".feather"
# df = pd.read_csv(filename,delimiter=delim)
df = pf.read_feather(filepath)
# print(df.info())
# Remove empty values
print('Removing patients with empty values')
df = df.dropna()
print('{} patients left after removal'.format(len(df)))
# Convert 0/1 columns to boolean columns
df_cat = df.select_dtypes(include='category').columns.difference(["dood"])
df[df_cat] = df[df_cat].astype(str)
for column in df:
    if column not in not_boolean_columns:
        df[column] = df[column].map({'0': "no", '1': "yes"})
df["dood"] = df["dood"].astype('int8')
print(df.info())
print(df)

patients = 500

if patients is not None:
    print('Selecting a subset of {} patients'.format(patients))
    df = df.iloc[0:patients, :]

# user configuration
task_name = "discovery"
target_type = "numeric"
max_depth = 5

# load class and fit to data
model = SSDC(task=task_name, max_depth=max_depth)
model.fit(df)
print("model measures : " + str(model.measures) + "\n")
print(model)
print("model rule_sets : " + str(model.rule_sets) + "\n")
