import pyarrow.feather as pf
from upsetplot import generate_counts
from upsetplot import plot
from matplotlib import pyplot

import pandas as pd

from tableone import TableOne
from subgroups import TARGET

DATA = "augmented_data_apriori.feather"
DATA_LOS = "COVIDdataset2022-07-11_w_los-processed.feather"

data = pf.read_feather(DATA)
# remove index if in columns (comes from python serialization into feather)
try:
    data.drop('index', axis=1, inplace=True)
except KeyError:
    pass

data_los = pf.read_feather(DATA_LOS)
# remove index if in columns (comes from python serialization into feather)
try:
    data_los.drop('index', axis=1, inplace=True)
except KeyError:
    pass

data['los_icu_total'] = data_los['los_icu_total'].astype(float)
data['los_hosp_total'] = data_los['los_hosp_total'].astype(float)

sg_cols = list(data.loc[:, data.columns.str.startswith('sg')].columns)
for col in sg_cols:
    print(col)
    print("size: {}".format(len(data[data[col] == 1])))
    print("n(dood): {}".format(len(data[(data[col] == 1) & (data['dood'] == 1)])))
    print("p(dood): {:0.3f}".format(len(data[(data[col] == 1) & (data['dood'] == 1)])/len(data[data[col] == 1])))
    print("Avg. LoS ICU: {:0.2f} ({:0.2f})"
          .format(data[data[col] == 1]['los_icu_total'].mean(), data[data[col] == 1]['los_icu_total'].std()))
    print("Avg. LoS hospital: {:0.2f} ({:0.2f})"
          .format(data[data[col] == 1]['los_hosp_total'].mean(), data[data[col] == 1]['los_hosp_total'].std()))
    print()
