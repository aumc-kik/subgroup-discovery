import pyarrow.feather as pf
import pandas as pd

from tableone import TableOne
from subgroups import TARGET

TAB = "tab1.html"
# DATA = "220217-134103-results-processed.feather"
DATA = "COVIDdataset2022-07-11-tab1.feather"
DATA_DATES = "COVIDdataset2022-07-11.csv"

SURGERY_VALUES = {
    # 3 no more in data dictionary for adm_type at https://www.stichting-nice.nl/dd/#16: merged with 2
    # (since 3 equivalent to 2 we consider it as a surgery patient)
    'adm_type': [2, 3, 4],
    # CFR https://www.stichting-nice.nl/dd/#356
    'ref_spec': [9, 12, 13, 23, 29, 36, 39, 44, 50]
}

data = pf.read_feather(DATA)
# remove index if in columns (comes from python serialization into feather)
try:
    data.drop('index', axis=1, inplace=True)
except KeyError:
    pass

print(data.info())

# Correlations
corr = data.corr()

# Table 1
columns = list(data.columns.values)
categoricals = list(data.select_dtypes(include='category').columns.values)
if 'finale_covid_status' in columns and 'finale_covid_status' not in categoricals:
    categoricals.append('finale_covid_status')
if 'gender' not in categoricals:
    categoricals.append('gender')
# GCS used as numeric but reported as categorical as more informative
categoricals.append('nice_gcs_low')
tab1 = TableOne(data, columns, categoricals, TARGET, pval=True)

# Prevalence
non_survivors = data[TARGET].astype(int).sum()
survivors = len(data) - non_survivors
prevalence = non_survivors/len(data)

# Check dates of finally included patients
df = pd.read_csv(DATA_DATES)
# Exclude patients without minimal dataset
df = df[df['MDSbekend'] == 1]
# Clean adm_type based on adm_src
# 1. fix wrong types
wrong_adm_type = ((df['adm_type'] == 2) | (df['adm_type'] == 4)) \
                 & ((df['adm_source'] == 3) | (df['adm_source'] == 4))
print("Correcting {} admission types based on admission source (setting them to 1 - Medical)".format(
    df[wrong_adm_type].shape[0]))
df.loc[wrong_adm_type, 'adm_type'] = 1
wrong_adm_type = (df['adm_type'] == 1) & ((df['adm_source'] == 2) | (df['adm_source'] == 1))
print("Correcting {} admission types based on admission source (setting them to 2 - Emergency surgical)".format(
    df[wrong_adm_type].shape[0]))
df.loc[wrong_adm_type, 'adm_type'] = 2
# 2. make missing adm_type medical admission unless adm_source is 1 or 2 (from operating room)
# (only patients from the operation room can have a surgical admission type)
# keep others as missing as it is not known if it is an emergency (2) or planned (4) surgery
null_adm_type_and_medical_src = df['adm_type'].isnull() & ((df['adm_source'] != 1.0) | (df['adm_source'] != 2.0))
df.loc[null_adm_type_and_medical_src, 'adm_type'] = 1

# Exclude surgery patients
print("Excluding surgery patients")
for surgery_feat in SURGERY_VALUES.keys():
    for surgery_value in SURGERY_VALUES[surgery_feat]:
        df = df.loc[df[surgery_feat] != surgery_value]
print('{} patients left after removal'.format(len(df)))

with open(TAB, 'w') as fn:
    print("<h1>Table 1</h1>", file=fn)
    print("<p><b>Prevalence:</b> {}% - {} survivors (0), {} non-survivors (1)</p>"
          .format(prevalence*100, survivors, non_survivors)
          , file=fn)
    print("<p>Patients admitted from {} to {} </p>"
          .format(df['eerste_ic_opname'].min(), df['eerste_ic_opname'].max())
          , file=fn)
    print(tab1.tabulate(tablefmt="html"), file=fn)
    # print("<p></p>", file=fn)
    print("<h1>Correlation</h1>", file=fn)
    print(corr.style.background_gradient(cmap='coolwarm').set_precision(2).render(), file=fn)
    print("<h1>Two-way crosstabs</h1>", file=fn)
    print("<p>Two-way crosstabs between the outcome and each of the predictor variables (only categoricals)" +
          "to locate any cell with zero observations indicating that variable leads to the perfect separation issue</p>"
          , file=fn)
    # Crosstabs only for categories
    for c in categoricals:
        print(pd.crosstab(data[TARGET], data[c]).to_html(),  file=fn)
        print("<p></p>", file=fn)
