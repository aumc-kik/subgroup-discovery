---
title: "R Notebook"
output: html_notebook
---
COVIDZWART
# Libraries
```{r, results='hide'} 
# install.packages("tidyverse")
# install.packages("naniar")
# install.packages("Hmisc")
# install.packages("rpart")
# install.packages("rpart.plot")
# install.packages("tableone")
# install.packages("arrow")

library("dplyr")
library("tidyverse")
library("skimr")
library("lubridate")
library("naniar")
library("Hmisc")
library("rpart")
library("rpart.plot")
library("rattle")
library("tableone")
library("treeClust")
library("boot")
library("arrow")
```
# Load
```{r, results='hide'}
df.raw <- tibble(read.csv("COVIDdataset2022-07-11.csv"))
```
## Add
```{r, results='hide'}
apgroepen <- read.csv("apacheiv.csv")

apgroepen <- apgroepen %>%
  mutate(APGroep = as.factor(APGroep),
         nice_ap4_heavy_idx = as.integer(factor(apgroepen$APGroep))) %>%
  rename(nice_ap4_heavy = Apache.ID)

df.raw <- df.raw %>%
  left_join(apgroepen, by = "nice_ap4_heavy")
```
## Cleanse
```{r}
df.raw[df.raw == -99] <- NA
df.raw[df.raw == 9999] <- NA
```
# View raw
## Summary
```{r, eval=FALSE} 
summary(df.raw)
```
## Missings
```{r, eval=FALSE}
gg_miss_upset(df.raw)
```
# Convert
```{r} 
df.convert <- df.raw

df.convert$eerste_zkh_opname   <- lubridate::as_date(df.convert$eerste_zkh_opname)
df.convert$eerste_ic_opname    <- lubridate::as_date(df.convert$eerste_ic_opname)
df.convert$laatste_zkh_ontslag <- lubridate::as_date(df.convert$laatste_zkh_ontslag)
df.convert$laatste_ic_ontslag  <- lubridate::as_date(df.convert$laatste_ic_ontslag)

# What are the integer predictors with 2 distinct values? we use this to make fcts
dichs <- df.convert %>%
  dplyr::select_if(is.numeric) %>%
  dplyr::select(where(~ n_distinct(., na.rm=T) == 2)) %>%
  names()

# What are the integer predictors with > 2 distinct values? we use this to make cats
cats <- df.convert %>%
  dplyr::select_if(is.integer) %>%
  dplyr::select(!contains('min') & !contains('max') & !contains('low') ) %>%
  dplyr::select(where(~ n_distinct(., na.rm=T) > 2)) %>%
  summarise_all(n_distinct) %>%
  t() %>%
  .[order(.[,1]),] %>%
  names()

cats <- cats[!cats %in% c("verschillende_ics",
                          "verschillende_zkh",
                          "nice_age",
                          "length",
                          "weight",
                          "nice_ap4_aps",
                          "nice_ap4_score",
                          "fio2",
                          "paco2",
                          "pao2")]

fcts <- c(dichs, cats)
          
# legacy - we used this in the first analyses -> if dichs and cats are correct, this can be removed
# facts <- c(dichs,"nice_ap4_heavy","nice_ap4_heavy_idx","regionid","re_adm","ref_spec","adm_source","adm_type","plan_adm","ap4diag1","ap4diag2","cardioGroep","nice_pat_type","zhtrans","dood","icu_death")

df.convert <- df.convert %>%
  mutate(across(all_of(fcts), factor)) 
```
## Add
Add golf
```{r}
df.convert <- df.convert %>%
  mutate(golf = case_when(eerste_ic_opname %within% interval(ymd("2020-02-01"), ymd("2020-05-15")) ~ 1,
                          eerste_ic_opname %within% interval(ymd("2020-10-01"), ymd("2020-11-30")) ~ 2,
                          eerste_ic_opname %within% interval(ymd("2020-12-01"), ymd("2021-01-31")) ~ 3,
                          eerste_ic_opname %within% interval(ymd("2021-02-01"), ymd("2021-06-30")) ~ 4,
                          eerste_ic_opname %within% interval(ymd("2021-07-01"), ymd("2021-09-30")) ~ 5,
                          eerste_ic_opname %within% interval(ymd("2021-10-01"), now()) ~ 6,
                          TRUE ~ 0))

df.convert$golf <- as.factor(df.convert$golf)
```
Add dichotomous bili and albim - indicates if measured or not
```{r}
# df.convert <- df.convert %>%
#   mutate(album_measured = case_when(is.na(album_min) & is.na(album_max) ~ 0,
#                            TRUE ~ 1),
#          bili_measured = case_when(is.na(bili) ~ 0,
#                            TRUE ~ 1))
# 
# df.convert$album_measured <- as.factor(df.convert$album_measured)
# df.convert$bili_measured <- as.factor(df.convert$bili_measured)
```
# Filter
Verwijder patient 28846 -> heeft bijna alleen NAs.
```{r}
df.filter <- df.convert %>%
  filter(pid != "28846")
# Remove patients w/o minimal dataset
df.filter <- df.convert %>%
  filter(MDSbekend == 1)
```
# Select
## Dataset
Pas deze selectie toe als de analyse alleen variabelen bij opname bevat.
```{r, eval=FALSE}
hrs24 <- c(
   "laatste_ic_ontslag"  ,"laatste_zkh_ontslag" ,"mech_ventil_24"      ,"ac_ren_fail"         ,"confirm_infection"   ,"vasdrug"            
  ,"nice_ap4_excluded"   ,"nice_ap4_score"      ,"nice_ap4_prob"       ,"verschillende_ics"   ,"verschillende_zkh"   ,"beademingsduur_uren"
  ,"eye_low"             ,"motor_low"           ,"verbal_low"          ,"heartrate_min"       ,"heartrate_max"       ,"resprate_min"       
  ,"resprate_max"        ,"syst_min"            ,"syst_max"            ,"meanbl_min"          ,"meanbl_max"          ,"temp_min"           
  ,"temp_max"            ,"urine_24"            ,"paco2"               ,"fio2"                ,"nice_pao2_fio2"      ,"a_ado2"             
  ,"pao2"                ,"ph_min"              ,"wbc_min"             ,"wbc_max"             ,"creat_min"           ,"creat_max"          
  ,"potas_min"           ,"potas_max"           ,"sodium_min"          ,"sodium_max"          ,"bicarb_min"          ,"bicarb_max"         
  ,"urea"                ,"bili"                ,"ht_min"              ,"ht_max"              ,"hb_min"              ,"hb_max"             
  ,"album_min"           ,"album_max"           ,"throm_min"           ,"gluc_min"            ,"gluc_max"            ,"nice_ap4_aps"       
  ,"nice_a_ado2"         ,"nice_gcs_low"        ,"hosp_discharged_to" 
)

# df.filter <- df.filter %>%
#   dplyr::select(-all_of(hrs24))
```
## Predictors
```{r}
# gebaseerd op predictor selectie email van Nicolette 21/12/02
excl <- c("pid",
          "eerste_zkh_opname",
          "eerste_ic_opname",
          "laatste_zkh_ontslag",
          "laatste_ic_ontslag",
          "beademingsduur_uren",  # after QC -> niet beschikbaar in eerste 24u
          "length",
          "weight",
          "nice_ap4_heavy",
          "regionid",
          "ap4diag1",
          "ap4diag2",
          "APGroep",
          "zhtrans",
          "nice_ap4_excluded",
          "nice_ap4_score",     # included as predictor to assess prognostic value -> appears in the tree?
          "nice_ap4_prob",
          "verschillende_zkh",
          "eye_low",
          "motor_low",
          "verbal_low",
          "a_ado2",
          "nice_ap4_aps",
          "cardiogroep",
          "nice_pat_type",
          "hosp_discharged_to",
          "nice_ap4_heavy_idx",
          "dood",
          "icu_death",
          "los_icu_total",
          "los_hosp_total",
          # "bili",
          "hb_min",
          "hb_max",
          "X",  # corresponding to unnamed column in CSV not useful
          # "album_min",
          # "album_max"
          "MDSbekend")

preds <- names(df.filter)[!names(df.filter) %in% excl]
outcomes <- c("dood")

df.select <- df.filter %>%
  dplyr::select(any_of(c(preds,outcomes)))
```
# View selected
## Summary
```{r, eval=FALSE}
summary(df.select)
```
## Missings
```{r,eval=FALSE}
gg_miss_var(df.select)
gg_miss_upset(df.select, nsets = n_var_miss(df.select), nintersects = NA)
```
## Histograms
```{r,eval=FALSE}
pdf("histograms.pdf", 8, 8)
hist.data.frame(df.select)
dev.off()
```
## Table one
```{r,eval=FALSE}
CreateTableOne(vars = names(df.select)[!names(df.select) %in% outcomes] , 
               data = df.select, 
               factorVars = names(Filter(is.factor, df.select)),
               strata = c("dood"),
               includeNA = TRUE,
               addOverall = TRUE)
```
## Write feather
```{r, eval=FALSE}
write_feather(df.select,"COVIDdataset2022-07-11.feather")
```
