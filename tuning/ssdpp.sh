DATASET=../COVIDdataset2022-07-11-processed.feather
ALGORITHM=SSDPP
OUTPUT_PREFIX=tuning_results_COVIDdataset2022-07-11/$ALGORITHM
mkdir -p $OUTPUT_PREFIX
for SD  in 5 6 7 8 9 10
do
  for BW in 25 50 100
  do
    echo python ../subgroups.py $DATASET algorithm $ALGORITHM -sd $SD -bw $BW #| tee $OUTPUT_PREFIX/$ALGORITHM-SD-$SD-BW-$BW.txt
    python ../subgroups.py $DATASET algorithm $ALGORITHM -sd $SD -bw $BW #| tee $OUTPUT_PREFIX/$ALGORITHM-SD-$SD-BW-$BW.txt
    cp subgroups.log $OUTPUT_PREFIX/$ALGORITHM-SD-$SD-BW-$BW.log
  done
done
exit 0