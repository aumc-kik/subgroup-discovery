DATASET=../COVIDdataset2022-07-11-processed.feather
ALGORITHM=APRIORI
OUTPUT_PREFIX=tuning_results_COVIDdataset2022-07-11/$ALGORITHM
mkdir -p $OUTPUT_PREFIX
for SN  in 5 10 25 50 75 100
do
  for SD in 5 6 7 8 9 10
  do
    echo python ../subgroups.py $DATASET algorithm $ALGORITHM -sn $SN -sd $SD #| tee $OUTPUT_PREFIX/$ALGORITHM-SN-$SN-SD-$SD.txt
    python ../subgroups.py $DATASET algorithm $ALGORITHM -sn $SN -sd $SD #| tee $OUTPUT_PREFIX/$ALGORITHM-SN-$SN-SD-$SD.txt
    cp subgroups.log $OUTPUT_PREFIX/$ALGORITHM-SN-$SN-SD-$SD.log
  done
done
exit 0