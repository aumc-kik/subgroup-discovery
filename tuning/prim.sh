DATASET=../COVIDdataset2022-07-11-processed.feather
ALGORITHM=PRIM
OUTPUT_PREFIX=tuning_results_COVIDdataset2022-07-11/$ALGORITHM
mkdir -p $OUTPUT_PREFIX
for ALPHA  in 0.03 0.04 0.05 0.06 0.08 0.1
do
  for BETA in 0.03 0.04 0.05 0.06 0.08 0.1
  do
    echo python ../subgroups.py $DATASET algorithm $ALGORITHM -a $ALPHA -b $BETA #| tee $OUTPUT_PREFIX/$ALGORITHM-a-$ALPHA-b-$BETA.txt
    python ../subgroups.py $DATASET algorithm $ALGORITHM -a $ALPHA -b $BETA #| tee $OUTPUT_PREFIX/$ALGORITHM-a-$ALPHA-b-$BETA.txt
    cp subgroups.log $OUTPUT_PREFIX/$ALGORITHM-a-$ALPHA-b-$BETA.log
  done
done
exit 0