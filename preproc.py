import os
import logging
from enum import Enum

import pyarrow.feather as pf

import rpy2.robjects as ro
from rpy2.robjects import pandas2ri
from rpy2.robjects.conversion import localconverter
from rpy2.robjects.packages import importr

# Expected format is feather
LOG = 'preproc.log'
# IN_FILE = "220217-134103-results.feather"
IN_FILE = "COVIDdataset2022-07-11.feather"
# OUT_FILE = "220217-134103-results-processed.feather"
# TODO With list of dfs imputation not serialized correclty
#  (still missing in the files though not till the last print in the main)
# OUT_FILE = "COVIDdataset2022-07-11-processed-{}.feather"
OUT_FILE = "COVIDdataset2022-07-11-processed.feather"
OUT_FILE_TAB1 = "COVIDdataset2022-07-11-tab1.feather"

EXCLUDED = ['plan_adm', 'confirm_infection', 'finale_covid_status', 'verschillende_ics']
TARGET = "dood"

# Set to true to clean adm_type based on adm_src
CLEAN_ADM_TYPE = True

# Set to false to exclude surgery patients
SURGERY = False
# Surgery values are categoricals (express as char)
SURGERY_VALUES = {
    # 3 no more in data dictionary for adm_type at https://www.stichting-nice.nl/dd/#16: merged with 2
    # (since 3 equivalent to 2 we consider it as a surgery patient)
    'adm_type': ['2', '3', '4'],
    # CFR https://www.stichting-nice.nl/dd/#356
    'ref_spec': ['9', '12', '13', '23', '29', '36', '39', '44', '50']
}
# Drop variables with over MISSING_THRESHOLD % of missing values
# Set to one to keep all columns
MISSING_THRESHOLD = 0.4

# Possible imputation methods
Imputation_method = Enum('Imputation_method', 'complete_cases mice')
# Selected imputation (one of the imputation methods above)
# Ignore what below with Imputation_method.none
IMPUTATION = Imputation_method.mice
MICE_METHOD = 'pmm'
MICE_M = 5

# Variables which have one unique value over 99% frequent are considered near zero variance (and thus excluded)
MAX_UNIQUE = 0.99


def threshold_counts(s, threshold=0):
    counts = s.value_counts(normalize=True, dropna=False)
    if (counts >= threshold).any():
        return False
    return True


def mice_imputation(df):
    utils = importr('utils')
    pandas2ri.activate()
    # convert the pandas dataframe to a R dataframe
    with localconverter(ro.default_converter + pandas2ri.converter):
        r_frame = ro.conversion.py2rpy(df)
    # load mice package
    mice = importr('mice')
    # load complete function
    complete = ro.r['complete']

    tmp = mice.mice(r_frame, m=MICE_M, method=MICE_METHOD, remove_collinear=False)

    df = complete(tmp, 1)
    with localconverter(ro.default_converter + pandas2ri.converter):
        df = ro.conversion.rpy2py(df)
    imputed_data = df.reset_index()

    # TODO With list of dfs imputation not serialized correclty
    #  (still missing in the files though not till the last print in the main)
    # imputed_data = []
    # for i in range(MICE_M):
    #     df = complete(tmp, i)
    #     with localconverter(ro.default_converter + pandas2ri.converter):
    #         df = ro.conversion.rpy2py(df)
    #     df = df.reset_index()
    #     imputed_data.append(df.copy())

    return imputed_data


def impute_data(df):
    if IMPUTATION == Imputation_method.mice:
        res = mice_imputation(df)
    else:
        # Remove all patients with empty values (select only complete cases)
        print('Removing patients with empty values')
        df = df.dropna()
        print('{} patients left after removal'.format(len(df)))
        # TODO With list of dfs imputation not serialized correclty
        #  (still missing in the files though not till the last print in the main)
        # res = [df]
        res = df

    return res

logging.basicConfig(level=logging.INFO, format='%(message)s')
logger = logging.getLogger()
logger.addHandler(logging.FileHandler(LOG, 'w'))
# redirect the print as log
print = logger.info

try:
    print('Reading {}'.format(IN_FILE))
    df = pf.read_feather(IN_FILE)
except:
    os.system('cls' if os.name == 'nt' else 'clear')
    print("Error while loading data file, please check if the file location is correct.")
    print()
    print("Provided filepath:", IN_FILE)
    print("This file should be in the same folder as the script (root location).")
    print()
    raise

print('File read: {} patients and {} variables'.format(len(df), len(df.columns)))
print("{}".format(df.info()))
print(df)

# Sanity check TARGET has to be in the data
assert TARGET in df.columns

df.drop(EXCLUDED, axis=1, inplace=True)
print('Removed variables: {}'.format(EXCLUDED))

missing = df.isnull().sum() / df.shape[0]
print("Missing frequencies")
print(missing)
print("Missing frequencies > {}: {}".format(MISSING_THRESHOLD, list(missing[missing > MISSING_THRESHOLD].index.values)))


if CLEAN_ADM_TYPE:
    # Clean adm_type based on adm_src
    # 1. fix wrong types
    wrong_adm_type = ((df['adm_type'] == '2') | (df['adm_type'] == '4')) \
                     & ((df['adm_source'] == '3') | (df['adm_source'] == '4'))
    print("Correcting {} admission types based on admission source (setting them to 1 - Medical)".format(
        df[wrong_adm_type].shape[0]))
    df.loc[wrong_adm_type, 'adm_type'] = '1'
    wrong_adm_type = (df['adm_type'] == '1') & ((df['adm_source'] == '2') | (df['adm_source'] == '1'))
    print("Correcting {} admission types based on admission source (setting them to 2 - Emergency surgical)".format(
        df[wrong_adm_type].shape[0]))
    df.loc[wrong_adm_type, 'adm_type'] = '2'
    # 2. make missing adm_type medical admission unless adm_source is 1 or 2 (from operating room)
    # (only patients from the operation room can have a surgical admission type)
    # keep others as missing as it is not known if it is an emergency (2) or planned (4) surgery
    null_adm_type_and_medical_src = df['adm_type'].isnull() & ((df['adm_source'] != 1.0) | (df['adm_source'] != 2.0))
    df.loc[null_adm_type_and_medical_src, 'adm_type'] = '1'

if not SURGERY:
    # Exclude surgery patients
    print("Excluding surgery patients")
    for surgery_feat in SURGERY_VALUES.keys():
        for surgery_value in SURGERY_VALUES[surgery_feat]:
            df = df.loc[df[surgery_feat] != surgery_value]
    print('{} patients left after removal'.format(len(df)))

if IMPUTATION == Imputation_method.complete_cases:
    # First remove incomplete cases then near zero vars
    dfs = impute_data(df)
    # ### Near zero vars ####
    # See https://www.r-bloggers.com/2014/03/near-zero-variance-predictors-should-we-remove-them/
    near_zero_var = df.apply(threshold_counts, threshold=MAX_UNIQUE)
    print("Features removed because of near zero variance (one unique value over {}% frequent): {}"
          .format(MAX_UNIQUE, df.columns[~near_zero_var]))

    # TODO With list of dfs imputation not serialized correclty
    #  (still missing in the files though not till the last print in the main)
    # only one DF
    # dfs[0] = dfs[0].loc[:, near_zero_var]
    dfs = dfs.loc[:, near_zero_var]
else:
    # First remove near zero vars then impute
    # ### Near zero vars ####
    # See https://www.r-bloggers.com/2014/03/near-zero-variance-predictors-should-we-remove-them/
    near_zero_var = df.apply(threshold_counts, threshold=MAX_UNIQUE)
    print("Features removed because of near zero variance (one unique value over {}% frequent): {}"
          .format(MAX_UNIQUE, df.columns[~near_zero_var]))
    df = df.loc[:, near_zero_var]

    # #### Correlation ####
    # Create correlation matrix
    # corr_matrix = df[df.columns.difference(subgrp_vars)].corr().abs()
    # upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))
    # # Find features with correlation greater than MAX_CORR
    # to_drop = [column for column in upper.columns if any(upper[column] > MAX_CORR)]
    # print("Features  removed because of correlation greater than {}: {}".format(MAX_CORR, to_drop))
    # df.drop(to_drop, axis=1, inplace=True)

    # To generate Tab 1 with missing but final pop included
    df.reset_index().to_feather(OUT_FILE_TAB1)
    dfs = impute_data(df)

# TODO With list of dfs imputation not serialized correclty
#  (still missing in the files though not till the last print in the main)
# for i, df in enumerate(dfs):
#     print(df)
#     print("{}".format(df.info()))
#     print("Processed data serialized into {}".format(OUT_FILE.format(i)))
#     df.to_feather(OUT_FILE.format(i))
print(dfs)
print("{}".format(dfs.info()))
print("Processed data serialized into {}".format(OUT_FILE))
dfs.to_feather(OUT_FILE)
