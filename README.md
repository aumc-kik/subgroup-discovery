# Subgroup discovery
Simple framework for subgroup discovery on COVID19 patients, 
developed on synthetic data. The framework is based on 
[pysubgroup](https://pysubgroup.readthedocs.io/en/latest/) 
(see also [this paper](http://www.ecmlpkdd2018.org/wp-content/uploads/2018/09/749.pdf)).

## Run 

`python subgroups.py [datafile path] [mode] [algorithm]`

- `[mode]`: Specify what analysis should be done, choose from the options below
  - `compare` Compare performance supported algorithms
  - `algorithm` Return results of Subgroup Discovery algorithm
- `[algorithm]`: Specify algorithm for 'algorithm' mode, 
   choose from the supported algorithms below
  - `PRIM` Perform Subgroup Discovery with PRIM algorithm
  - `APRIORI` Perform Subgroup Discovery with APRIORI algorithm
  - `BEAM` Perform Subgroup Discovery with BEAM algorithm
  - `SSDPP` Perform Subgroup Discovery with SSD++ algorithm

Example 1:

`python subgroups.py 220217-134103-results.feather compare`

Compare PRIM, APRIORI, BEAM, and SDD++ with respect to different measures 
(coverage, support, significance,  WRAcc,  confidence,  redundancy)

Example 2:

`python subgroups.py 220217-134103-results.feather algorithm APRIORI`

Show the results (subgroups) of the a-priori algorithm for subgroup discovery 

## Data

Data are expected in the 
[feather](https://arrow.apache.org/docs/python/feather.html) format.

The framework was used for subgroup analysis of COVID-19 patients' data from the 
[NICE registry](https://www.stichting-nice.nl). 
Data is available under stringent conditions as described on the 
[NICE website](https://www.stichting-nice.nl/extractieverzoek.jsp) (in Dutch).
Variables are described in the [data dictionary](https://www.stichting-nice.nl/dd/#start)

## Models

We propose a framework that offers the following model for subgroup discovery:

- [PRIM](https://link.springer.com/article/10.1023/A:1008894516817)
- [Beam search](https://link.springer.com/article/10.1007/BF00116835)
- [Apriori-SD](https://link.springer.com/chapter/10.1007/978-3-540-45231-7_22)
- [SSD++](https://link.springer.com/chapter/10.1007/978-3-030-67658-2_2)

The framework is designed to enable extension by adding further models.
