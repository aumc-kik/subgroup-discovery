# Subgroup Discovery for COVID-19 patients
# M.Y. Kingma
# Student Master Medical Informatics
# Amsterdam UMC, Universiteit van Amsterdam
import sys
import os
import logging
from pandas.api.types import is_numeric_dtype

#### Script settings ####
LOG = 'subgroups.log'
TARGET = "dood"
AUGMENTED_DATA = "augmented_data.feather"

# Amount of patients taken from the dataset used for subgroup discovery. Set to None to use all patients.
# PATIENTS = 400
PATIENTS = None
# Amount of variables used for subgroup discovery. Set to None to use all variables.
# variables = 64 # Max 64
VARIABLES = None

# Select the desired amount of subgroups (only supported by APRIORI and BEAM algorithms from PySubgroup)
SUBGROUP_NUM = 5
# Select max amount of rules/conditions per group (only supported by APRIORI, BEAM and SSDPP algorithms)
SELECTOR_DEPTH = 10
# PRIM parameters
THRESHOLD_BOX = 0.1
ALPHA = 0.1
# SSDPP parameters
N_CUTPOINTS = 1
BEAM_WIDTH = 25

# Declaration of all categorical (type= category) that are not boolean
NOT_BOOL_COLS = ['finale_covid_status', 'ref_spec', 'adm_source', 'adm_type', 'aantalchronisch', 'golf']

#### Global functions ####
def print_usage():
    print("Usage: python subgroups.py datafile_path mode [algorithm] [-sn] [-sd] [-a] [-b] [--augment-data]")
    print("\n")
    # Assumed file is ready to be applied for subgroup discovery (cleaning, preproc, variable selected, etc.)
    print("datafile_path: Filepath of *.feather file for Subgroup Discovery")
    print("    You can use '[filename].feather' if the file is placed in the same directory as this script.")
    print("\n")
    print("mode: Specify what analysis should be done, choose from the options below")
    print("    'compare' = compare performance supported algorithms")
    print("    'algorithm' = return results of Subgroup Discovery algorithm")
    print("\n")
    print("[algorithm]: Specify algorithm for 'algorithm' mode, choose from the supported algorithms below")
    print("    'PRIM' = perform Subgroup Discovery with PRIM algorithm")
    print("    'APRIORI' = perform Subgroup Discovery with APRIORI algorithm")
    print("    'BEAM' = perform Subgroup Discovery with BEAM algorithm")
    print("    'SSDPP' = perform Subgroup Discovery with SSD++ algorithm")
    print("\n")
    print("[-sn]: Specify the number of subgroup for APRIORI. Must be followed by an integer value.")
    print("\n")
    print("[-sd]: Specify the selector depth (max. number of rules) for APRIORI. Must be followed by an integer value.")
    print("\n")
    print("[-a]: Specify the alpha parameter for PRIM. Must be followed by an float value in (0,1].")
    print("\n")
    print("[-b]: Specify the beta parameter for PRIM. Must be followed by an float value.")
    print("\n")
    print("[--augment-data]: Works with 'algorithm' mode, if given generate a new file where new variables are added "
          + "that track membership to 1 or more subgroups for each patient")
    print("\n")
    print("\n")


#### Main program ####
def main(filepath, mode, algorithm=None, augment=False, subgroup_num=SUBGROUP_NUM, selector_depth=SELECTOR_DEPTH,
         alpha=ALPHA, threshold_box=THRESHOLD_BOX, beam_width=BEAM_WIDTH):
    try:
        from math import log
        import pandas as pd
        import pyarrow.feather as pf
        import pysubgroup as ps
        from PRIM.src import PRIM
        import SSDpp.src
        import numpy as np
    except:
        os.system('cls' if os.name == 'nt' else 'clear')
        print("\n")
        print("Missing required modules, please install all modules before running.")
        print("\n")
        raise
    pd.options.display.max_columns = None

    #### Main functions ####
    def create_subgroup_specific_results(index, rules, WRAcc, size_sg, pos_sg, group_df, algorithm):
        '''
        Create result dictionary with subgroup specific results

        param index: index of subgroup
        param rules: rule representation of subgroup
        param WRAcc: Weighted Relatice Accuracy statistic of subgroup
        param size_sg: size of subgroup
        param pos_sg: amount of positive target variables in subgroup
        param group_df: dataframe of subgroup
        param algorithm: specify algorithm for error logging

        returns: subgroup result dictionary with quality measure statistics
        '''

        try:
            # Declare dict for subgroup
            group = {}

            # Set group specific results
            group['index'] = index
            group['rules'] = rules
            group['WRAcc'] = WRAcc
            group['size'] = size_sg
            group['size_dataset'] = instances_dataset
            group['positives_sg'] = pos_sg
            group['positives_dataset'] = positives_dataset
            group['coverage'] = size_sg / instances_dataset
            group['support'] = group['positives_sg'] / instances_dataset
            group['confidence'] = group['positives_sg'] / size_sg
            negatives_subgroup = size_sg - pos_sg

            if negatives_subgroup == 0:
                group['significance'] = 2 * (
                        pos_sg * log(pos_sg / (positives_dataset * (size_sg / instances_dataset)))
                )
            else:
                group['significance'] = 2 * (
                        pos_sg * log(pos_sg / (positives_dataset * (size_sg / instances_dataset))) +
                        negatives_subgroup * log(
                    negatives_subgroup / (negatives_dataset * (size_sg / instances_dataset)))
                )

            # Add dataframe of subgroup
            group['df'] = group_df

            return group
        except ValueError:
            os.system('cls' if os.name == 'nt' else 'clear')
            print("\n")
            print(
                f"Error occured in calculating subgroup specific results for {algorithm} algorithm, the below values are used:")
            print("\n")
            print("index:", index)
            print("rules:", rules)
            print("WRAcc:", WRAcc)
            print("size_sg:", size_sg)
            print("pos_sg:", pos_sg)
            print(
                "pos_sg={}, positives_dataset={}, size_sg={}, instances_dataset={}, negatives_subgroup={}, negatives_dataset={}"
                    .format(pos_sg, positives_dataset, size_sg, instances_dataset, negatives_subgroup
                            , negatives_dataset))
            print("\n")
            group['significance'] = np.NAN
            # raise

    def analize_overall_results(subgroups, algorithm):
        '''
        Analyse overall results from specific algorithm subgroup set

        param subgroups: subgroup discovery result dictionary

        returns: result dictionary with array of subgroups and dict with overall quality measures
        '''

        try:
            dfs = []
            for group in subgroups['subgroups']:
                if group is not None:
                    dfs.append(group['df'])

            all_covered = pd.concat(dfs)
            all_covered = all_covered.drop_duplicates()
            all_positive = all_covered[all_covered[TARGET] == True].shape[0]
            subgroups['results']['overall_coverage'] = all_covered.shape[0] / instances_dataset
            subgroups['results']['overall_support'] = all_positive / instances_dataset
        except:
            os.system('cls' if os.name == 'nt' else 'clear')
            print("\n")
            print(f"Error occured while creating overall subgroup dataframes for {algorithm} algorithm.")
            print("\n")
            raise

        total_subgroups = len(subgroups['subgroups'])
        total_coverage = 0
        total_support = 0
        total_WRAcc = 0
        total_confidence = 0
        total_significance = 0
        max_WRAcc = 0
        redundancy, avg_rule_length = calculate_redundancy_and_avg_rule_length(subgroups["subgroups"], algorithm)

        try:
            for group in subgroups['subgroups']:
                total_coverage += group['coverage']
                total_support += group['support']
                total_WRAcc += group['WRAcc']
                total_confidence += group['confidence']
                total_significance += group['significance']
                if group['WRAcc'] > max_WRAcc:
                    max_WRAcc = group['WRAcc']

            subgroups['results']['average_coverage'] = total_coverage / total_subgroups
            subgroups['results']['average_support'] = total_support / total_subgroups
            subgroups['results']['average_WRAcc'] = total_WRAcc / total_subgroups
            subgroups['results']['average_confidence'] = total_confidence / total_subgroups
            subgroups['results']['average_significance'] = total_significance / total_subgroups
            subgroups['results']['max_WRAcc'] = max_WRAcc
            subgroups['results']['redundancy'] = redundancy
            subgroups['results']['total_subgroups'] = total_subgroups
            subgroups['results']['average_rule_length'] = avg_rule_length

        except:
            os.system('cls' if os.name == 'nt' else 'clear')
            print("\n")
            print(f"Error occured while calculating overall results for {algorithm} algorithm.")

    def print_subgroup_results(subgroupResults, algorithm):
        '''
        Print algorithm specific results of subgroups

        param subgroupResults: list of subgroups

        prints: subgroup results of specified algorithm
        '''
        # print(subgroupResults["subgroups"][0]["rules"].__str__())
        statistics_table = {
            'rules': [],
            'WRAcc': [],
            'size': [],
            'positives_sg': [],
            'positives_dataset': [],
            'coverage': [],
            'support': [],
            'confidence': [],
            'significance': [],
        }
        for group in subgroupResults["subgroups"]:
            for key, value in group.items():
                if key in statistics_table.keys():
                    if key == "rules" and algorithm != "PRIM":
                        statistics_table[key].append(value.__str__().replace("==", " == "))
                    else:
                        statistics_table[key].append(value)

        statistics_table["pos_sg"] = statistics_table["positives_sg"]
        statistics_table["pos_data"] = statistics_table["positives_dataset"]
        del statistics_table["positives_sg"]
        del statistics_table["positives_dataset"]

        statistics_table = pd.DataFrame(statistics_table)

        os.system('cls' if os.name == 'nt' else 'clear')

        print(f"Result table for {algorithm} algorithm.")
        print("\n")
        print(statistics_table.to_string())
        print("\n")

    def calculate_redundancy_and_avg_rule_length(subgroups, algorithm):
        '''
                Computes redundancy and averave rule length of subgroups

                param subgroups: index of subgroup

                param algorithm: algorithm to which subgroups refers to

                returns: subgroup results of specified algorithm, average rule length

        '''
        redundantGroups = 0
        ruleLengths = []
        for i in range(len(subgroups)):
            group = subgroups[i]
            if algorithm == "PRIM" or algorithm == "SSDPP":
                rules = group["rules"] if group is not None else ""
            else:
                rules = group["rules"].__str__()

            ruleLength = len(rules.split(" AND "))
            ruleLengths.append(ruleLength)
            for j in range(len(subgroups)):
            # for j in range(i + 1, len(subgroups)):
                compareGroup = subgroups[j]
                if group is not None and compareGroup is not None and group["index"] != compareGroup["index"]:
                    compareIsSubset = len(compareGroup["df"].merge(group["df"], left_index=True, right_index=True)) == len(compareGroup["df"])
                else:
                    compareIsSubset = False
                if algorithm == "PRIM" or algorithm == "SSDPP":
                    compareRules = compareGroup["rules"] if compareGroup is not None else ""
                else:
                    compareRules = compareGroup["rules"].__str__()

                compareRuleLength = len(compareRules.split(" AND "))

                if compareIsSubset and compareRuleLength >= ruleLength:
                    redundantGroups += 1
                    # If a subgroup redundant with more than one other subgroups count it only once
                    break

        return (redundantGroups / len(subgroups)) * 100, np.mean(ruleLengths)

    def analyse_subgroups_PRIM(df):
        ''' Using PRIM module cloned from https://github.com/martinsps/PRIM '''

        # Create PRIM instance
        try:
            p = PRIM.PRIM(df, TARGET, True, ordinal_columns={}, alpha=alpha, threshold_box=threshold_box)
        except:
            os.system('cls' if os.name == 'nt' else 'clear')
            print("\n")
            print("Error occurred in defining PRIM instance, Check the provided data and script settings.")
            print("\n")
            raise

        # Execute algorithm
        try:
            p.execute()
        except:
            os.system('cls' if os.name == 'nt' else 'clear')
            print("\n")
            print("Error occured while performing subgroup dicovery with APRIORI algorithm.")
            print("\n")
            raise

        ## Result analysis
        # Declare index and result dictionary
        subgroupsPRIM = {'subgroups': [], 'results': {}}
        index = 0

        try:
            # Loop over subgroups (boxes) in PRIM instance
            for box in p.boxes:

                # Create subset containing subgroup patients
                subgroup = p.apply_box(box, df)

                # Calculate subgroup size
                instances_subgroup = subgroup.shape[0]

                # Calculate amount of positive target variable in subgroup
                positives_subgroup = subgroup[subgroup[TARGET] == True].shape[0]

                # Probability of positive target variable in subgroup
                p_subgroup = np.divide(positives_subgroup, instances_subgroup)

                # Calculate Weighted Relative Accuracy statistic
                WRAccQF = (instances_subgroup / instances_dataset) ** 1 * (p_subgroup - p_dataset)

                # Create empty rule string
                ruleString = ""

                # Loop over rules for subgroup (box)
                for rule in box.boundary_list:

                    # Create rule representation string
                    boxRule = rule.__str__()

                    # Change syntax to make rule more readable
                    boxRule = boxRule.replace(' != True', ' == False').replace(' != False', ' == True')

                    # Append rule to rule string
                    if len(ruleString):
                        ruleString += " AND "
                    ruleString += boxRule

                # Create subgroup specific results
                group = create_subgroup_specific_results(index, ruleString, WRAccQF, instances_subgroup,
                                                         positives_subgroup, subgroup, "PRIM")

                # Increment index and append group to result dictionary
                index += 1
                subgroupsPRIM['subgroups'].append(group)

        except:
            os.system('cls' if os.name == 'nt' else 'clear')
            print("\n")
            print("Error occured in analysis of results from pysubgroup package {algorithm} algorithm")
            print("\n")
            raise

        return subgroupsPRIM

    def analyse_subgroups_SSDPP(df):
        ''' Using SSD++ module cloned from https://github.com/HMProenca/SSDpp-numeric '''

        task_name = "discovery"
        target_type = "numeric"

        # The SSD++ implementation used requires that target is the last columns in the dataframe
        if df.columns[-1] != TARGET:
            # TODO not tested yet
            df = df[[c for c in df if c != TARGET] + TARGET]
            print("To use SSD++, {} must be the last column. Rearranged columns in the dataframe to match requirement"
                  .format(TARGET))
        if not is_numeric_dtype(df[TARGET]):
            print("Warning: SSD++ is designed for numeric targets")

        # Adjust data format to comply with SSD requirements
        df[TARGET] = df[TARGET].astype('int8')
        df_cat = df.select_dtypes(include='category').columns
        NOT_BOOL_COLS.append(TARGET)
        yes_no_cols = df_cat.difference(NOT_BOOL_COLS)
        for c in yes_no_cols:
            df[c] = df[c].map({'True': "no", 'False': "yes"})
        df[yes_no_cols] = df[yes_no_cols].astype(str)
        bool_cols = df.select_dtypes(include='bool').columns
        df[bool_cols] = df[bool_cols].astype(str)
        for c in bool_cols:
            df[c] = df[c].map({'True': "no", 'False': "yes"})

        print("SDDP++ types")
        print("{}".format(df.info()))

        # load class and fit to data
        model = SSDpp.SSDC(task=task_name, max_depth=selector_depth, n_cutpoints=N_CUTPOINTS, beam_width=beam_width)
        model.fit(df)

        ## Result analysis
        # Declare index and result dictionary
        subgroupsSSDPP = {'subgroups': [], 'results': {}}
        index = 0

        for subgroup in model.get_subgroups(df):
            # Calculate subgroup size
            instances_subgroup = subgroup.shape[0]

            # Calculate amount of positive target variable in subgroup
            positives_subgroup = subgroup[subgroup[TARGET] == 1].shape[0]

            # Probability of positive target variable in subgroup
            p_subgroup = np.divide(positives_subgroup, instances_subgroup)

            # Calculate Weighted Relative Accuracy statistic
            WRAccQF = (instances_subgroup / instances_dataset) ** 1 * (p_subgroup - p_dataset)

            # Create rule representation string
            ruleString = model.get_rule(index)

            # Create subgroup specific results
            group = create_subgroup_specific_results(index, ruleString, WRAccQF, instances_subgroup, positives_subgroup,
                                                     subgroup, "SSDPP")

            # Increment index and append group to result dictionary
            index += 1
            subgroupsSSDPP['subgroups'].append(group)

        return subgroupsSSDPP

    #### Data load ####
    # Import data file 
    try:
        print('Reading {}'.format(filepath))
        df = pf.read_feather(filepath)
    except:
        os.system('cls' if os.name == 'nt' else 'clear')
        print("Error while loading data file, please check if the file location is correct.")
        print("\n")
        print("Provided filepath:", filepath)
        print("This file should be in the same folder as the script (root location).")
        print("\n")
        raise

    print('File read: {} patients and {} variables'.format(len(df), len(df.columns)))
    print("{}".format(df.info()))

    # Remove index if in columns (comes from python serialization into feather)
    # So missing in original data file from R, but present if data are serialized by preproc.py
    try:
        df.drop('index', axis=1, inplace=True)
    except KeyError:
        pass

    # Keep track of data types to set them back into augmented data
    data_types = df.dtypes.apply(lambda x: x.name).to_dict()

    try:
        # FIXME In final code from MK. Moved in preprc.py for completness.
        #  Keep it here? (should test if any of the implemented method works with missing and how well)
        #  At now kept because chosen robustness over performance (assuming code won't work with missing
        #  and likely does not affect much performance)
        # Remove empty values
        print('Removing patients with empty values')
        df = df.dropna()
        print('{} patients left after removal'.format(len(df)))

        bool_cols = df.select_dtypes(include=['category']).columns.difference(NOT_BOOL_COLS)
        for c in bool_cols:
            df[c] = df[c].map({'0': False, '1': True})
            df[c] = df[c].astype('bool')

        # Convert int32 columns to float64 columns
        int_cols = df.select_dtypes(include=['int32']).columns
        df[int_cols] = df[int_cols].astype("float64")

        print("APRIORI types")
        print("{}".format(df.info()))
        
        if PATIENTS is not None and VARIABLES is not None:
            # Create subset of data according to settings
            print('Selecting a subset of {} patients and {} variables'.format(PATIENTS, VARIABLES))
            sub_df = df.iloc[0:PATIENTS, 0:VARIABLES]
        elif PATIENTS is not None:
            print('Selecting a subset of {} patients'.format(PATIENTS))
            sub_df = df.iloc[0:PATIENTS, :]
        elif VARIABLES is not None:
            print('Selecting a subset of {} variables'.format(PATIENTS, VARIABLES))
            sub_df = df.iloc[:, 0:VARIABLES]
        else:
            sub_df = df

        if TARGET not in sub_df.columns:
            # Add target variable (dood/death)
            sub_df = df.assign(dood=df[0:PATIENTS][TARGET])
    except:
        os.system('cls' if os.name == 'nt' else 'clear')
        print("\n")
        print("Error occured while setting datatypes on dataframe. Check the provided data.")
        print("\n")
        raise

    ## Subset constants
    try:
        # Amount of patients in subset
        instances_dataset = len(sub_df)

        # Amount of patients with positive (or negative) target variable
        positives_dataset = sub_df[sub_df[TARGET] == True].shape[0]
        negatives_dataset = instances_dataset - positives_dataset

        # Probability of positive target variable in subset
        p_dataset = positives_dataset / instances_dataset
    except:
        os.system('cls' if os.name == 'nt' else 'clear')
        print("\n")
        print("Error occured while defining constants of the dataset. Check the provided data.")
        print("\n")
        raise

    #### PySubgroup package ####
    try:
        # Declare target variable and searchspace
        target = ps.BinaryTarget(TARGET, True)
        searchspace = ps.create_selectors(sub_df, ignore=[TARGET])

        # Declare task
        task = ps.SubgroupDiscoveryTask(
            sub_df,
            target,
            searchspace,
            result_set_size=subgroup_num,
            depth=selector_depth,
            qf=ps.WRAccQF())  # Quality measure subgroups
    except:
        os.system('cls' if os.name == 'nt' else 'clear')
        print("\n")
        print("Error occured during setup of PySubgroup module.")
        print("\n")
        raise

    # Ignore error created by PySubgroup in true_divide function
    np.seterr(divide='ignore', invalid='ignore')

    if mode == "compare" or algorithm == "BEAM":
        try:
            # Execute task with BEAM algorithm
            resultDataBeam = ps.BeamSearch(beam_width_adaptive=True).execute(task)
        except:
            print("\n")
            print("Error occured while performing subgroup dicovery with BEAM algorithm.")
            print("\n")
            raise

    if mode == "compare" or algorithm == "APRIORI":
        try:
            # Execute task with APRIORI algorithm
            resultDataApriori = ps.Apriori().execute(task)
        except:
            os.system('cls' if os.name == 'nt' else 'clear')
            print("\n")
            print("Error occured while performing subgroup dicovery with APRIORI algorithm.")
            print("\n")
            raise

    # Go to origional error settings
    np.seterr(divide='raise', invalid='raise')

    ## Result analysis
    def analyse_subgroups_pysubgroup(resultObject, data, algorithm):
        '''
        Analyse results from PySubgroup package

        param resultObject: SubgroupDiscoveryResult object returned from task execution
        param data: dataset, or subset used by task execution

        returns: result dictionary with array of subgroups and dict with overall quality measures
        '''

        # Declare index and result dictionary
        index = 0
        subgroups = {'subgroups': [], 'results': {}}

        try:
            # Loop over subgroups in resultObject
            for (q, sg, stats) in resultObject.results:

                # Calculate subgroup size and cover array
                # Cover array: true/false array mapping if patient is part of subgroup,
                # for complete dataset used in the subgroup discovery
                try:
                    if hasattr(sg, "representation"):
                        cover_arr = sg.representation
                        size_sg = sg.representation.sum()
                    else:
                        cover_arr, size_sg = ps.get_cover_array_and_size(sg, len(data), data)

                    # Add cover array to dataset
                    data = data.assign(in_subgroup=cover_arr).copy()

                    # Create subset containing subgroup patients following cover array
                    group_df = data[data['in_subgroup'] == True].copy()

                    # Remove in_subgroup column from group dataframe and dataset
                    group_df.drop('in_subgroup', axis=1, inplace=True)
                    data.drop('in_subgroup', axis=1, inplace=True)

                    # Calculate amount of positive target variable in subgroup
                    positives_subgroup = group_df[group_df[TARGET] == True].shape[0]
                except:
                    os.system('cls' if os.name == 'nt' else 'clear')
                    print("\n")
                    print(f"Error occured while creating subgroup dataframe for {algorithm} algorithm.")
                    print("Subgroup:", sg)
                    print("\n")
                    raise

                # Create subgroup specific results
                group = create_subgroup_specific_results(index, sg, q, size_sg, positives_subgroup, group_df, algorithm)

                # Increment index and append group to result dictionary
                index += 1
                subgroups['subgroups'].append(group)

        except:
            os.system('cls' if os.name == 'nt' else 'clear')
            print("\n")
            print(f"Error occured in analysis of results from pysubgroup package {algorithm} algorithm")
            print("\n")
            raise

        return subgroups

    def augment_data_with_subgroups(df, algorithm, subgroups):
        idx = 1
        sg_vars = []
        for group in subgroups['subgroups']:
            if group is not None:
                # Add a variable with the membership to the current subgroup
                sg_vars.append('sg{}'.format(idx))
                df[sg_vars[idx - 1]] = df.index.isin(group['df'].index)
            else:
                df[sg_vars[idx - 1]] = False
            idx += 1

        # Restore original dtypes (the one read from feather file)
        print(df.head())
        # print("!!!!Types before restore!!!!!")
        # print("{}".format(df.info()))
        if algorithm == "SSDPP":
            df = df.replace({"yes": 1, "no": 0})
            df[sg_vars] = df[sg_vars].astype(int)
        else:
            bool_cols = df.select_dtypes(include='bool').columns
            df[bool_cols] = df[bool_cols].astype(int)
        df[sg_vars] = df[sg_vars].astype('category')
        df = df.astype(data_types)
        print(df.head())
        print("{}".format(df.info()))
        print("Augmented data serialized into {}".format(AUGMENTED_DATA))
        df.reset_index().to_feather(AUGMENTED_DATA)

    # Call analyse function for BEAM and APRIORI algorithms
    if mode == "compare":
        subgroupsBEAM = analyse_subgroups_pysubgroup(resultDataBeam, sub_df, "BEAM")
        subgroupsAPRIORI = analyse_subgroups_pysubgroup(resultDataApriori, sub_df, "APRIORI")
    elif algorithm == "BEAM":
        subgroupsBEAM = analyse_subgroups_pysubgroup(resultDataBeam, sub_df, "BEAM")
        print_subgroup_results(subgroupsBEAM, "BEAM")
    elif algorithm == "APRIORI":
        subgroupsAPRIORI = analyse_subgroups_pysubgroup(resultDataApriori, sub_df, "APRIORI")
        print_subgroup_results(subgroupsAPRIORI, "APRIORI")

    #### PRIM module ####
    if mode == "compare" or algorithm == "PRIM":
        subgroupsPRIM = analyse_subgroups_PRIM(sub_df)

        if algorithm == "PRIM":
            print_subgroup_results(subgroupsPRIM, "PRIM")

    #### SSD++ module ####
    if mode == "compare" or algorithm == "SSDPP":
        subgroupsSSDPP = analyse_subgroups_SSDPP(sub_df)

        if algorithm == "SSDPP":
            print_subgroup_results(subgroupsSSDPP, "SSDPP")

    #### Overall statistics ####
    if mode == "compare":
        # Compute stats for all algorithms
        resultObjects = [subgroupsPRIM, subgroupsBEAM, subgroupsAPRIORI, subgroupsSSDPP]
        methods = ['PRIM', 'BEAM', 'APRIORI', 'SSDPP']
    else:
        # Compute stats only for the specified algorithm
        if algorithm == 'APRIORI':
            resultObjects = [subgroupsAPRIORI]
        elif algorithm == 'BEAM':
            resultObjects = [subgroupsBEAM]
        elif algorithm == 'PRIM':
            resultObjects = [subgroupsPRIM]
        else:
            resultObjects = [subgroupsSSDPP]
        methods = [algorithm]

    # Loop over resultObjects
    for index, object in enumerate(resultObjects):
        # Define overall statistics
        analize_overall_results(object, methods[index])

    # Define statistics table outline
    statistics_table = {
        'Method': [],
        'coverage_avg': [],
        'coverage_overall': [],
        'support_avg': [],
        'support_overall': [],
        'significance_avg': [],
        'WRAcc_avg': [],
        'WRAcc_max': [],
        'confidence_avg': [],
        'redundancy': [],
        'total_subgroups': [],
        'rule_length_avg': []
    }

    try:
        # Fill table with results
        for method, result in zip(methods, resultObjects):
            statistics_table['Method'].append(method)
            if 'average_coverage' in result['results']:
                statistics_table['coverage_avg'].append(result['results']['average_coverage'])
            else:
                statistics_table['coverage_avg'].append(np.NAN)
            if 'overall_coverage' in result['results']:
                statistics_table['coverage_overall'].append(result['results']['overall_coverage'])
            else:
                statistics_table['coverage_overall'].append(np.NAN)
            if 'average_support' in result['results']:
                statistics_table['support_avg'].append(result['results']['average_support'])
            else:
                statistics_table['support_avg'].append(np.NAN)
            if 'overall_support' in result['results']:
                statistics_table['support_overall'].append(result['results']['overall_support'])
            else:
                statistics_table['support_overall'].append(np.NAN)
            if 'average_WRAcc' in result['results']:
                statistics_table['WRAcc_avg'].append(result['results']['average_WRAcc'])
            else:
                statistics_table['WRAcc_avg'].append(np.NAN)
            if 'max_WRAcc' in result['results']:
                statistics_table['WRAcc_max'].append(result['results']['max_WRAcc'])
            else:
                statistics_table['WRAcc_max'].append(np.NAN)
            if 'average_confidence' in result['results']:
                statistics_table['confidence_avg'].append(result['results']['average_confidence'])
            else:
                statistics_table['confidence_avg'].append(np.NAN)
            if 'redundancy' in result['results']:
                statistics_table['redundancy'].append(result['results']['redundancy'])
            else:
                statistics_table['redundancy'].append(np.NAN)
            if 'average_significance' in result['results']:
                statistics_table['significance_avg'].append(result['results']['average_significance'])
            else:
                statistics_table['significance_avg'].append(np.NAN)
            if 'total_subgroups' in result['results']:
                statistics_table['total_subgroups'].append(result['results']['total_subgroups'])
            else:
                statistics_table['total_subgroups'].append(np.NAN)
            if 'average_rule_length' in result['results']:
                statistics_table['rule_length_avg'].append(result['results']['average_rule_length'])
            else:
                statistics_table['rule_length_avg'].append(np.NAN)

    except:
        os.system('cls' if os.name == 'nt' else 'clear')
        print("\n")
        print("Error occured while filling the result table with overall results")
        print("\n")
        raise

    # Convert to DataFrame
    statistics_table = pd.DataFrame(statistics_table)
    print(statistics_table)

    if augment:
        print("Augmenting data with subgroups membership")
        # augment used when there is only 1 resultObject in resultObjects
        augment_data_with_subgroups(sub_df, algorithm, resultObjects[0])


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, format='%(message)s')
    logger = logging.getLogger()
    logger.addHandler(logging.FileHandler(LOG, 'w'))
    # redirect the print as log
    print = logger.info

    if len(sys.argv) == 3 and sys.argv[2] == "compare":
        main(sys.argv[1], sys.argv[2])
    elif len(sys.argv) == 1 or sys.argv[1] in ["help", "h", "-h", "-help", "--h", "--help"]:
        os.system('cls' if os.name == 'nt' else 'clear')
        print("\n")
        print("This script performs a Subgroup Discovery analysis on a specified *.feather datafile.")
        print("Supported algorithms: PRIM, APRIORI, BEAM, SSDPP")
        print("\n")
        print_usage()
    elif len(sys.argv) == 4 and sys.argv[2] == "algorithm" and sys.argv[3] in ["APRIORI", "PRIM", "BEAM", "SSDPP"]:
        main(sys.argv[1], sys.argv[2], sys.argv[3])
    elif len(sys.argv) == 5 and sys.argv[2] == "algorithm" and sys.argv[3] in ["APRIORI", "PRIM", "BEAM", "SSDPP"] \
            and sys.argv[4] == "--augment-data":
        main(sys.argv[1], sys.argv[2], sys.argv[3], augment=True)
    elif len(sys.argv) == 8 and sys.argv[2] == "algorithm" and sys.argv[3] == "APRIORI" and sys.argv[4] == "-sn" \
            and sys.argv[6] == "-sd":
        try:
            sn = int(sys.argv[5])
            sd = int(sys.argv[7])
        except ValueError:
            print_usage()
            sys.exit("Subgroup number and selector depth must be integers")

        main(sys.argv[1], sys.argv[2], sys.argv[3], subgroup_num=sn, selector_depth=sd)
    elif len(sys.argv) == 9 and sys.argv[2] == "algorithm" and sys.argv[3] == "APRIORI" and sys.argv[4] == "-sn" \
            and sys.argv[6] == "-sd" and sys.argv[8] == "--augment-data":
        try:
            sn = int(sys.argv[5])
            sd = int(sys.argv[7])
        except ValueError:
            print_usage()
            sys.exit("Subgroup number and selector depth must be integers")

        main(sys.argv[1], sys.argv[2], sys.argv[3], subgroup_num=sn, selector_depth=sd, augment=True)
    elif len(sys.argv) == 8 and sys.argv[2] == "algorithm" and sys.argv[3] == "PRIM" and sys.argv[4] == "-a" \
            and sys.argv[6] == "-b":
        try:
            a = float(sys.argv[5])
            b = float(sys.argv[7])
        except ValueError:
            print_usage()
            sys.exit("Alpha and beta (threshold box) must be floats")

        if a <= 0.0 or a > 1.0:
            sys.exit("Alpha must be in (0,1]")

        main(sys.argv[1], sys.argv[2], sys.argv[3], alpha=a, threshold_box=b)
    elif len(sys.argv) == 9 and sys.argv[2] == "algorithm" and sys.argv[3] == "PRIM" and sys.argv[4] == "-a" \
            and sys.argv[6] == "-b" and sys.argv[8] == "--augment-data":
        try:
            a = float(sys.argv[5])
            b = float(sys.argv[7])
        except ValueError:
            print_usage()
            sys.exit("Alpha and beta (threshold box) must be floats")

        if a <= 0.0 or a > 1.0:
            sys.exit("Alpha must be in (0,1]")

        main(sys.argv[1], sys.argv[2], sys.argv[3], alpha=a, threshold_box=b, augment=True)
    elif len(sys.argv) == 8 and sys.argv[2] == "algorithm" and sys.argv[3] == "SSDPP" and sys.argv[4] == "-sd"\
            and sys.argv[6] == "-bw":
        try:
            sd = int(sys.argv[5])
            bw = int(sys.argv[7])
        except ValueError:
            print_usage()
            sys.exit("Selector depth and beam width must be integers")

        main(sys.argv[1], sys.argv[2], sys.argv[3], selector_depth=sd, beam_width=bw)
    elif len(sys.argv) == 9 and sys.argv[2] == "algorithm" and sys.argv[3] == "SSDPP" and sys.argv[4] == "-sd" \
            and sys.argv[6] == "-bw" and sys.argv[8] == "--augment-data":
        try:
            sd = int(sys.argv[5])
            bw = int(sys.argv[7])
        except ValueError:
            print_usage()
            sys.exit("Selector depth and beam width must be integers")

        main(sys.argv[1], sys.argv[2], sys.argv[3], selector_depth=sd, beam_width=bw, augment=True)
    else:
        os.system('cls' if os.name == 'nt' else 'clear')
        print("\n")
        print(
            "Problem with script arguments, please check if you run the script properly. "
            "Run 'python subgroups.py help' for help")
        print("\n")
        print_usage()
